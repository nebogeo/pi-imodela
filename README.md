# pi-imodela

Untangling the internals of the iModela IM-01 CNC machine so we can
have a simple free software driver to run on a raspberry pi. It seems
fairly simple as the usb device accepts plain g-code, no data back
though, so it's a lot of guesswork.

It seems all the difficult/important/safety things are taken care of
by the imodela firmware, e.g. shutting down when the motor overheats
so you don't need to worry too much about that.

Current status:

* cnc.py [filename.gcode]

Filters g-code for commands that hang the imodela, homes the CNC and
sends the resulting commands to /dev/usb/lp0

This script uses homing values as the local frame of reference (G54)
in a file called "home.txt" with X Y Z on 3 lines eg:

    5
    10
    -5

This will be created and updated using a joypad to position the
imodela (so we don't need a screen). The important thing is that it is
persistent so we can shut things down and it matches the job currently
stuck to the spoil plate.

This can also be set on the commandline via:

    ./cnc.py home 1 2 3

Also:

    ./cnc.py go_home

# Feeds/speeds tested

## PCB cutting using pcb2gcode

* 40 mm/min

## Wood cutting using [jscut](http://jscut.org)

Need to insert M3 code to actually turn the motor on!

Need to convert feeds to floating point (add .0) to get the imodela to
read them as mm.

These tests are with balsa wood material.

* 3mm end mill

* 10 mm/min cut + plunge
* 3-ish mm pass depth

Motor overheats

* 10 mm/min cut + plunge
* 0.5 mm pass depth

Works but hella slow, not stressing the motor

* 40 mm/min cut + plunge
* 1.0 mm pass depth

Works well, still a touch slow but only problems were jamming and
slowing due to collection of material in deeper sections.

# Checklist for cutting Viruscraft Construction Kit pieces

jscut settings for balsa with 2mm end mill

  - all units mm
  - diameter:   2mm
  - pass depth: 1mm 
  - step over:  0.4mm
  - rapid:      1000
  - plunge:     40
  - cut:        40
  - gcode conversion: zero lower left
  - export to local file

Edit gcode:

  - add M3 to start motor clockwise
  - add M5 at the end to stop it
  - search replace F1000 to F1000.0 (so it's read as mm)
  - search replace F40 to F40.0

# Research notes: supported g-code 

Referring to reference from the [manual
pdf](https://www.imodela.co.uk/pdf/NC_Code_Reference_Manual.pdf) -
this covers all Roland equipment so not all of it is useful, but it
seems better than relying on generic g code documentation.

Sending commands via `echo "G00 X10.0" > /dev/usb/lp0` with the im-01
set to 'NC mode' in windows driver.

After starting the mill, wait for it to rehome and for the LED to stop
flashing, once constant it will accept commands.

Errors result in flashing LED and non-responsive mill, eventually
rejecting commands (buffer full?) and needing power cycle to reset.

## Commands tested

Move xyz:

    G00 X4.0 Y3.0 Z-1.0

IMPORTANT DISCOVERY: Numbers of the form 0.0 (mm?) are treated
differently to decimals only (thousands of inch?) 1000

Homing procedure:

Set current coordinate system to G54:

    G54

(move to desired location) Zero the coordinates for G54:
    
    G54 G92 X0.0 Y0.0 Z0.0

This works relative, so:

    G54 G92 X-20.0 Y0.0 Z0.0

Now G00 movements are relative to this position. 

    G53

Return to "machine" coordinate system, and seems to lose the homing
for G54 - so we need to be careful. Hmm, actually it seems like these
relative homing coordinates are lost if no commands are sent for about
5 seconds...

Alternative method (defined absolutely), same problems though:

    G10L2P 1 X10.0 Y0 Z0

Where 1 is the coordinate system (1=G54)

I'm guessing all this means that the homing coords need to be stored
persistently by the driver rather than internally to the machine - and
resent before any job?

