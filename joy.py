from evdev import InputDevice, categorize, ecodes
import time
import pickle

class cnc_control:
    def __init__(self,device_path):
        self.device_path=device_path
        self.deadzone=80 # ignore centre of joysticks - noisy
        self.speed=1
        self.speeds=[0.0001, 0.004, 0.01]
        self.current = [0.0,0.0,0.0]
        self.change = [0.0,0.0,0.0]
        self.last = [0.0,0.0,0.0]
        # set for imodela tested values
        self.minimum = [0.0,0.0,-26.0]
        self.maximum = [86.0,55.0,0.0]
        self.drill = False
        
    def send(self,gcode):
        print(gcode)
        cnc = open(self.device_path,"wb")
        cnc.write(gcode.encode("ascii", "ignore"))
        cnc.close()

    def deal_with_input(self,event):
        if event.type==1:
            if event.value==1:
                print(event)
                if event.code==292: # shoulder up
                    self.current[2]+=1
                if event.code==293: # shoulder down
                    self.current[2]-=1
                if event.code==296: # start
                    if self.drill:
                        self.send("M05")
                        self.drill=False
                    else:
                        self.send("M03 1000")
                        self.drill=True
                if event.code==297: # select
                    print(self.speeds[self.speed])
                    self.speed=(self.speed+1)%len(self.speeds)

        if event.type==3: 
            # directional pad buttons
            if event.code==0:
                if event.value==0:
                    self.current[0]-=1
                if event.value==255:
                    self.current[0]+=1
            if event.code==1:
                if event.value==0:
                    self.current[1]-=1
                if event.value==255:
                    self.current[1]+=1

            s = self.speeds[self.speed]
            
            # right joystick
            if event.code==2:
                self.change[1]=0
                v = 127-event.value
                v=-v
                if abs(v)>self.deadzone:
                    if v>0:
                        self.change[1]+=(v-self.deadzone)*s
                    else:
                        self.change[1]+=(v+self.deadzone)*s
            if event.code==5:
                self.change[0]=0
                v = 127-event.value
                v=-v
                if abs(v)>self.deadzone:
                    if v>0:
                        self.change[0]+=(v-self.deadzone)*s
                    else:
                        self.change[0]+=(v+self.deadzone)*s

    def update(self):
        #print(change)
        self.current[0]+=self.change[0]
        self.current[1]+=self.change[1]
        self.current[2]+=self.change[2]

        # check ranges
        if self.current[0]<self.minimum[0]: self.current[0]=self.minimum[0]
        if self.current[1]<self.minimum[1]: self.current[1]=self.minimum[1]
        if self.current[2]<self.minimum[2]: self.current[2]=self.minimum[2]

        if self.current[0]>self.maximum[0]: self.current[0]=self.maximum[0]
        if self.current[1]>self.maximum[1]: self.current[1]=self.maximum[1]
        if self.current[2]>self.maximum[2]: self.current[2]=self.maximum[2]
        
        if self.current[0]!=self.last[0] or self.current[1]!=self.last[1] or self.current[2]!=self.last[2]:
            self.send("G00 X%.2f Y%.2f Z%.2f\n"%(self.current[0],self.current[1],self.current[2]))
        
        self.last[0]=self.current[0]
        self.last[1]=self.current[1]
        self.last[2]=self.current[2]

#-----------------------------------------------------------
        
cnc = cnc_control("/dev/usb/lp0")
gamepad = InputDevice('/dev/input/event18')
print(gamepad)    
                        
while True:
    try:
        for event in gamepad.read():
            cnc.deal_with_input(event)
    except BlockingIOError:
        pass
    cnc.update()
    time.sleep(0.05)
