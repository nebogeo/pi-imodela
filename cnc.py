#!/usr/bin/env python3

import time
import pickle
import sys

# filter G04 G64

class cnc_control:
    def __init__(self,device_path):
        self.device_path=device_path
        self.deadzone=80 # ignore centre of joysticks - noisy
        self.speed=1
        self.speeds=[0.0001, 0.004, 0.01]
        self.current = [0.0,0.0,0.0]
        self.change = [0.0,0.0,0.0]
        self.last = [0.0,0.0,0.0]
        # set for imodela tested values
        self.minimum = [0.0,0.0,-26.0]
        self.maximum = [86.0,55.0,0.0]
        self.drill = False
        self.home_pos = [0.0,0.0,0.0]
        self.filtered_codes=["G04","G64"]
        self.load_home()

    def strip_comments(self, s):
        out = ""
        for c in s:
            if c==";": return out
            out+=c
        return out
        
    def send(self,gcode):
        gcode=self.strip_comments(gcode)
        if gcode.replace("\n","")=="": return
        start_code=gcode[0:3]
        if start_code in self.filtered_codes:
            print("ignoring: "+gcode)
            return
        cnc = open(self.device_path,"wb")
        cnc.write(gcode.encode("ascii", "ignore"))
        cnc.close()
        print(gcode.replace("\n",""))

    def set_home(self):
        self.home_pos=self.current
        home = open("home.txt","w")
        home.write(str(self.home_pos[0])+"\n")
        home.write(str(self.home_pos[1])+"\n")
        home.write(str(self.home_pos[2])+"\n")
        home.close()

    def go_home(self):
        self.change=[0.0,0.0,0.0]
        self.current[0]=self.home_pos[0] 
        self.current[1]=self.home_pos[1] 
        self.current[2]=self.home_pos[2] 
        self.update()

    def load_home(self):
        home = open("home.txt","r")
        self.home_pos[0]=float(home.readline())
        self.home_pos[1]=float(home.readline())
        self.home_pos[2]=float(home.readline())
        home.close()
        print("loaded home as: "+str(self.home_pos[0])+", "+str(self.home_pos[1])+", "+str(self.home_pos[2]))
        self.go_home()

    def prog_home(self):
        # program the registers for the G54 offset
        # the imodela is forgetful, so we need to set these before every job
        self.send("G10L2P 1 X"+str(self.home_pos[0])+" Y"+str(self.home_pos[1])+" Z"+str(self.home_pos[2])+"\n")
        self.send("G54\n")
        #self.send("G54 G92 X"+str(self.home_pos[0])+" Y"+str(self.home_pos[1])+" Z"+str(self.home_pos[2]))
        #self.send("G00 X0.0 Y0.0 Z0.0")        
        
    def execute(self,filename):
        self.prog_home()
        f = open(filename,"r")
        lines = f.readlines()
        f.close()
        for l in lines:
            #l=l.replace("\n","")
            #print(l)
            self.send(l)        
            #time.sleep()
            
    def update(self):
        #print(change)
        self.current[0]+=self.change[0]
        self.current[1]+=self.change[1]
        self.current[2]+=self.change[2]

        # check ranges
        if self.current[0]<self.minimum[0]: self.current[0]=self.minimum[0]
        if self.current[1]<self.minimum[1]: self.current[1]=self.minimum[1]
        if self.current[2]<self.minimum[2]: self.current[2]=self.minimum[2]

        if self.current[0]>self.maximum[0]: self.current[0]=self.maximum[0]
        if self.current[1]>self.maximum[1]: self.current[1]=self.maximum[1]
        if self.current[2]>self.maximum[2]: self.current[2]=self.maximum[2]
        
        if self.current[0]!=self.last[0] or self.current[1]!=self.last[1] or self.current[2]!=self.last[2]:
            self.send("G00 X%.2f Y%.2f Z%.2f\n"%(self.current[0],self.current[1],self.current[2]))
        
        self.last[0]=self.current[0]
        self.last[1]=self.current[1]
        self.last[2]=self.current[2]

#-----------------------------------------------------------
        
cnc = cnc_control("/dev/usb/lp0")

print(sys.argv[1])

if __name__=="__main__":
    if sys.argv[1]=="go_home":
        pass
    elif sys.argv[1]=="home":
        x=float(sys.argv[2])
        y=float(sys.argv[3])
        z=float(sys.argv[4])
        print("homing x:"+str(x)+" y:"+str(y)+" z:"+str(z))
        cnc.current[0]=x
        cnc.current[1]=y
        cnc.current[2]=z
        cnc.update()
        cnc.set_home()
    else:
        print("running: "+sys.argv[1])
        cnc.execute(sys.argv[1])
